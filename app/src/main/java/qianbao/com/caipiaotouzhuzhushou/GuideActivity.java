package qianbao.com.caipiaotouzhuzhushou;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liuyong
 * Data: 2018/6/20
 * Github:https://github.com/MrAllRight
 */

public class GuideActivity extends Activity {
    ViewPager vp;
    private GuideAdapter vpAdapter;
    private List<View> views;
    // 引导图片资源
    private int[] pics = {R.mipmap.guide1
    };
    View view;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.activity_guide);
        vp= (ViewPager) findViewById(R.id.viewpager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        views = new ArrayList<View>();

        // 为引导图片提供布局参数
        LinearLayout.LayoutParams mParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        // 初始化引导图片列表
        for (int i = 0; i < pics.length; i++) {
            ImageView iv = new ImageView(this);
            iv.setLayoutParams(mParams);
            iv.setImageResource(pics[i]);
            if (i == 3) iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickView();
                }
            });
            views.add(iv);
        }
        // 初始化Adapter
        vpAdapter = new GuideAdapter(views);
        vp.setAdapter(vpAdapter);
    }
    public void onClickView() {
        Intent intent=new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}