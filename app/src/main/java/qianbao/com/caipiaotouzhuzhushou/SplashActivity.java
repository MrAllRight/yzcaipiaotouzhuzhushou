package qianbao.com.caipiaotouzhuzhushou;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by liuyong
 * Data: 2018/6/20
 * Github:https://github.com/MrAllRight
 */
public class SplashActivity extends Activity {
    private int version = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        toNextPage();
    }

    private void initView() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();


    }

    /**
     * timer for test pause
     */

    void toNextPage() {
        version = BuildConfig.VERSION_CODE;
        /**
         * 使用handler来处理
         */
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initView();
            }

        }, 1000);

    }

}